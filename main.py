import glob
import os
import csv
import subprocess
import shlex
import math
import random

import cv2

import pandas as pd

import numpy as np

from keras.applications.inception_v3 import InceptionV3

from sklearn.linear_model import LogisticRegression

from sklearn.model_selection import train_test_split

from sklearn.metrics import classification_report

isTest = False



def extract_feature():
    base_model = InceptionV3(weights='imagenet', include_top=False, pooling="avg")
    # folders = ['inlier_train', 'outlier_train', "test"]
    folders = ["test"]

    for folder in folders:
        files = glob.glob(os.path.join(folder, '*.jpg'))
        files.sort()
        # print(files)
        if isTest:
            take_files = files[:66]
        else:
            take_files = files[:]
        if isTest:
            total_batch = 20
        else:
            total_batch = 2000
        for batch_idx in range(0, len(take_files), total_batch):
            print(batch_idx)
            if isTest:
                train_or_test = folder.split('/')[-1]
            else:
                train_or_test = folder.split('\\')[-1]
            subfiles = take_files[batch_idx:batch_idx + total_batch]
            imgs = []
            labels= []
            filenames = []
            features = []

            for file in subfiles:
                classname = get_class(file)
                file_name = get_filename(file)

                image = cv2.imread(file)
                image = cv2.resize(image, (299, 299))
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                # cv2.imwrite(file, image)
                # continue
                imgs.append(image)
                labels.append(classname)
                filenames.append(file_name)

            images, categories = normalise_images(imgs, labels)
            # batch_image = split_padded(images, 500)
            last_idx = -1
            if isTest:
                batch_count = 10
            else:
                batch_count = 500

            for idx in range(0, len(images), batch_count):
                feature = base_model.predict(images[idx:idx + batch_count])
                features.append(feature)
                # idx += batch_count

            features = np.array(features)

            features = np.vstack(features)

            feature_pd = pd.DataFrame(features)

            label_pd = pd.DataFrame(labels, columns=["label"])

            filename_pd = pd.DataFrame(filenames, columns=["filename"])

            data_pd = pd.concat([feature_pd, filename_pd, label_pd], axis=1)
            data_pd.to_csv("datas_" + folder + str(batch_idx) + ".csv")



def normalise_images(images, labels):
    # Convert to numpy arrays
    images = np.array(images, dtype=np.float32)
    labels = np.array(labels)

    # Normalise the images
    images /= 255

    return images, labels

def get_class(path):

    """Given a full path to a video, return its parts."""
    parts = path.split(os.path.sep)

    classname = parts[0]
    if classname == 'inlier_train':
        class_idx = 0
    else:
        class_idx = 1

    return class_idx

def get_filename(path):
    parts = path.split(os.path.sep)
    filename = parts[1]
    name = filename.split('.')[0]

    return name


def load_test_data():
    if isTest:
        lenth = 61
        step = 20
    else:
        lenth = 1
        step = 1

    data_list = []
    for idx in range(0, lenth, step):

        print(idx)
        filename = "datas_test" + str(idx) + '.csv'

        data = pd.read_csv(filename)
        data_list.append(data)
    datas = pd.concat(data_list, axis=0)

    return datas


def load_data(isTrain):
    if isTest:
        train_len = 61
        test_len = 61
    else:
        train_len = 4001
        test_len = 1

    if isTrain:
        lenth = train_len
    else:
        lenth = test_len
    data_list = []

    if isTest:
        step = 20
    else:
        step = 2000
    for idx in range(0, lenth, step):
        print(idx)
        if isTrain:
            filename = "datas_inlier_train" + str(idx) + '.csv'
        else:
            filename = "datas_outlier_train" + str(idx) + '.csv'
        data = pd.read_csv(filename)
        data_list.append(data)
    datas = pd.concat(data_list, axis=0)

    return datas

# extract_feature()
# exit()
data1 = load_data(True)
data2 = load_data(False)

test_data = load_test_data()
test_X = test_data.drop(test_data.columns[0], axis=1)
name_X = test_data['filename']
test_X = test_X.drop("filename", axis = 1)
test_X = test_X.drop("label", axis = 1)



datas = pd.concat([data1, data2], axis=0)
datas = datas.drop(datas.columns[0], axis=1)


y = datas['label']
X = datas.drop("label", axis = 1)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)



print(X_train)
print(y_train)
# exit()
clf = LogisticRegression(class_weight="balanced", solver="lbfgs").fit(X_train, y_train)
y_pred = clf.predict(X_test)
print(classification_report(y_test, y_pred))

# if isTest == False:
if True:
    clf = LogisticRegression(class_weight="balanced", solver="lbfgs").fit(X, y)
    y_pred = clf.predict(test_X)
    print(y_pred)
    pred_pd = pd.DataFrame(y_pred, columns=["Result"])

    name_pd = name_X.to_frame()
    name_pd = name_pd.rename(columns = {"filename":"ID"})

    result  = pd.concat([name_pd, pred_pd], axis=1)
    result.to_csv("submission.csv")
    print(result)